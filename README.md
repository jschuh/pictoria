# pictorIA

## Description
This repository collects the tools created in the pictorIA Huma-Num Consortium. For more information, visit [pictorIA](https://pictoria.hypotheses.org/).

## Contributing
Feel free to fork the project and submit your contributions via pull requests.

## Scientific Coordinators
Julien Schuh, Professor at the University of Paris Nanterre (UFR PHILLIA, CSLF EA 1586)

Anne-Violaine Szabados, Research Engineer (CNRS, InSHS, ArScAn-UMR7041)

Jean-Philippe Moreux, Scientific Expert at Gallica (National Library of France, Department of Cooperation, Digital Cooperation and Gallica Service) 

## License
MIT License
