# Tutoriel : Utilisation du Notebook sur Google Colab

## Introduction

Ce tutoriel vous guidera pas à pas pour utiliser un notebook sur Google Colab afin de segmenter des images en utilisant le modèle "Segment Anything" (SAM) développé par Facebook AI Research (FAIR). 
L'outil développé sous la forme de notebook est destiné à une démonstration: ainsi il n'est adapaté qu'à partir de fichiers de données spécifiques, ne permet de charger qu'une seule image du fichier et de ne sauvegarder qu'une seule annotation.

### Modèle "Segment Anything"

Le modèle "Segment Anything" est conçu pour segmenter automatiquement les objets dans une image. Il utilise un réseau de neurones convolutifs (CNN) pour prédire les masques des objets présents dans l'image. Le modèle est pré-entraîné sur un large ensemble de données et peut être utilisé pour des tâches de segmentation sans nécessiter de réentraînement.

### Fonctionnement du modèle

1. **Chargement de l'image** : L'image est chargée et convertie en un format compatible avec le modèle.
2. **Prédiction des masques** : Le modèle prédit les masques pour les objets présents dans l'image.
3. **Visualisation** : Les masques prédits sont superposés sur l'image d'origine pour visualiser les objets segmentés.

## Prérequis

Avant de commencer, assurez-vous d'avoir :
- Un compte Google.
- Accès à Google Colab.
- Le jeu de données pré-annoté Mandragore fourni par la BNF: https://api.bnf.fr/fr/mandragore-echantillon-segmente-2019. Il est téléchargeable dans la partie à droite de l'affiche web. Une version compressée est aussi disponible dans ce mêmle dépôt. Les fichiers qui nous interessent se situent dans le jeu de données: Segmentation2019/MiradorAnnotations/ après avoir désarchivé le fichier .ZIP.
Ce dossier contient 8 dossiers correspondant à 8 références du corpus de Mandragore: des fichiers .JSON contenant les annotations préalablement réalisées. Pour certaines références, 4 fichiers .JSON ont été réalisés, seuls ceux nommés "MiradorAnnotations_Enluminures.json" vont nous intéresser.

## Étapes

### 1. Installation des dépendances

La première étape consiste à installer les bibliothèques nécessaires, notamment celles pour le modèle SAM et ses dépendances.
Les poids du modèle se téléchargent dans un environnement temporaire (accessible, dans la barre de gauche quans vous cliquez sur l'icone du dossier)

### 2. Chargement du modèle

Ensuite, vous devez charger le modèle pré-entraîné "Segment Anything". Ce modèle est conçu pour segmenter automatiquement les objets dans une image. 
Le script est développé de manière à ce que les fichiers préalablement téléchargés soient chargés directement.

### 3. Chargement du fichier d'annotations .JSON

Après lancement de la cellule, vous pouvez choisir en local le fichier .JSON téléchargé (un fichier nommé 'MiradorAnnotations_Enluminures.json'). L'image et les labels correpondants seront récupérés via les identifiants sauvegardés dans le fichier de données

### 4. Chargement et segmentation d'une image

Une interface va afficher une image du fichier d'annotations avec ses labels correpondants grâce au widget. Vous pouvez choisir un label affiché en dessous de l'image et tracer l'annotation correspondante. Une fois vos annotations terminées, vous pouvez cliquer sur le bouton 'Submit'.

### 5. Visualisation des résultats

Après avoir lancé la cellule, le modèle pré-entraîné SAM va détecter l'objet à partir de vos annotations. Le téléchargement de l'image et l'inférence du modèle sont longs à charger (la cellule peut s'éxecuter pendant plusieures secondes voire minutes)


### 6. Sauvegarde de l'annotation dans le fichier .JSON

En dessous du label chargé va s'enregistrer la position du masqué détecté par le modèle SAM. Un nouveau fichier .JSON est sauvegardé dans l'architecture temporaire (accessilbe dans la colonne de gauche de l'écran) sous le nom "...edited.json. Vous pouvez télécharger ce fichier pour visualiser l'annotation sur votre ordinateur en local.

