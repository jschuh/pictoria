# Tutoriel: Utilisation du notebook d'appel aux API des bases de données

## Description

Ce tutoriel vous guidera pas à pas pour utiliser un notebook Google Colab afin de télécharger des images par titre à partir de trois bases de données: Gallica, Europeana et la base Joconde. À noter que pour cette dernière, seules les métadonnées sont téléchargées, les images correspondantes ne sont pas récupérées. L'outil réalisé effectue un lien vers les API de recherche et de récupération des bibliothèques numériques qui donnent à disposition leurs données. 
Vous devez lancer la première cellule de code. Les trois autres sont optionnelles: si vous ne souhaitez récupérer que les données d'Europeana par exemple, inutile de lancer la deuxième et la quatrième cellule.

### Les APIs

Une API, ou Interface de Programmation Applicative, est un ensemble de protocoles qui permettent à différents logiciels de communiquer entre eux. C'est comme une boîte à outils de fonctions et d'instructions pré-définies que les développeurs peuvent utiliser pour faciliter la création d'applications. 
- Ici, nous utilisons l'API de recherche (https://api.bnf.fr/fr/api-gallica-de-recherche) et de récupération IIIF (https://api.bnf.fr/fr/api-iiif-de-recuperation-des-images-de-gallica) de Gallica. Ces APIs permettent un accès aux données et aux métadonnées des documents numérisés présents dans Gallica. Les métadonnées incluent des informations telles que les titres des ouvrages, les auteurs, les dates de publication, etc. Des paramètres de requête spécifiques pour filtrer les résultats selon les besoins sont disponibles. En plus des métadonnées, l'API permet également d'accéder aux contenus numériques eux-mêmes, tels que les images de pages de livres, de journaux, de cartes, etc. 
- Nous utilisons aussi l'API de recherche d'Europeana (https://europeana.atlassian.net/wiki/spaces/EF/pages/2385739812) qui renvoie les métadonnées contenant les liens vers les images dans leurs serveurs IIIF.
- La troisième API utilisée est celle de la base Joconde (https://data.culture.gouv.fr/explore/dataset/base-joconde-extrait/api/?disjunctive.departement&disjunctive.manquant), c'est une API de recherche.

### Le protocole IIIF

Le protocole IIIF, ou International Image Interoperability Framework, est une initiative collaborative qui vise à améliorer l'interopérabilité des images numériques dans le domaine culturel et académique. Dans notre cas d'utilisation, il permet notamment l'accès aux images haute résolution de manière standardisée et interopérable. Cela signifie que l'on peut récupérer, consulter et zoomer sur des images détaillées sans perte de qualité, quel que soit le serveur ou le système utilisé.


## Les étapes

### Cellule 1 : Connexion à son compte Google Drive et choix du terme de recherche

Avant de lancer la première cellule, indiquez le dossier dans lequel vous souhaitez travailler: dans lequel les fichiers vont se télécharger. Si vous ne changez rien, un dossier 'API' sera créé à la racine de votre drive, dans lequel les téléchargements se feront.
Le deuxième champs à remplir est le terme de recherche que vous souhaitez utiliser pour balayer la base de données. Les accents ne doivent pas être ajoutés. La recherche se fait dans le titre des documents de la manière dont ils sont enregistrés dans la base de données.

### Cellule 2 : Récupération des images et métadonnées de Gallica

Cette cellule fait appel à l'API de Gallica, récupère les métadonnées via un flux JSON. Les liens vers les images sont récupérés dans ce flux et téléchargés de cette manière. Ensuite, les métadonnées sont transformées en format tableur pour être plus facilement accessibles et exploitables.

### Cellule 3 : Récupération des images et métadonnées d'Europeana

De la même manière que la cellule précédente, cette cellule fait appel à l'API d'Europeana, récupère les métadonnées via un flux JSON. Les liens vers les images sont récupérés dans ce flux et téléchargés de cette manière. Ensuite, les métadonnées sont transformées en format tableur pour être plus facilement accessibles et exploitables.

### Cellule 4 : Récupération des métadonnées de la base Joconde

L'API de la base Joconde est ici aussi appelée pour récupérer le flux de métadonnées correspondants à la recherche. Le format des métadonnées est aussi converti en tableur. 

Vous devriez avoir accès dans votre Google Drive à l'ensemble des fichiers téléchargés: les images de Gallica et Europeana ainsi que les métadonnées des 3 bases de données. 






