Tutoriel sur l'utilisation de Google Colaboratory pour l’extraction des images de pages de journaux

Fonctionnement de Google Colaboratory
1. Accès et création de Notebooks :
    * Vous pouvez accéder à Google Colaboratory en cliquant sur "New Notebook" sur cette page: https://colab.google/
    * A partir de la page du notebook ouvert, en haut à gauche cliquez sur "Fichier" => "Importer le notebook".
    * La fenêtre de navigation permets d’importer un notebook notamment via Google Drive ou votre ordinateur (en local). S’il est enregistré sur votre Drive ou sur votre ordinateur vous pouvez importer le fichier. Une autre solution est d’indiquer un lien vers un dépôt Github (nom du compte qui a publié).
2. Structure d'un Notebook :
    * Un notebook est composé de cellules qui peuvent contenir du texte ou du code (language Python).
    * Vous pouvez exécuter chaque cellule de code individuellement en cliquant sur le bouton "Exécuter" à gauche de la cellule ou en utilisant le raccourci clavier Shift+Enter. Si le logo de votre fenêtre est grise: la cellule est encore en train de fonctionner. Quand le logo repasse au jaune c’est que la cellule lancée a fini.
    * Les résultats des calculs et les sorties sont affichés directement en dessous de chaque cellule.
3. Utilisation des Packages et Dépendances :
    * Colab permet l'installation de packages Python directement dans les notebooks à l'aide de commandes comme !pip install ....
    * Les packages sont installés temporairement pour la session en cours et doivent être réinstallés à chaque nouvelle session.
AINSI: la première cellule doit toujours être lancée à l’ouverture d’un notebook. Généralement, les libraires à installer se situent dans cette cellule. 

Fonctionnement de l'outil d'extraction
1. Modèle de Détection de Mise en Page :
    * Le script utilise un modèle pré-entraîné pour détecter et extraire les images des pages de journaux
    * Ce modèle est configuré via un fichier de configuration (config.yaml) et des poids de modèle (model_final.pth) téléchargés depuis un dépôt Hugging Face. Ce modèle a été entraîné sur des journaux publiés sur Gallica (BNF) et datent principalement du début de XIXe siècle. 
2. Connexion à Google Drive :
    * Le script se connecte à Google Drive pour accéder aux données d'entrée (images à traiter) et pour sauvegarder les résultats (images extraites).
    * Cette connexion est établie via une autorisation OAuth et permet un accès direct aux fichiers stockés sur Google Drive.
3. Exportation des Fichiers :
    * Les fichiers exportés sont les régions d'intérêt extraites à partir des images traitées.
    * Chaque image est enregistrée comme une image séparée dans un répertoire spécifié sur Google Drive (extractions_path).
Pour cela, dans la troisième et dernière cellule du notebook, vous devez indiquer deux chemins qui mènent vers votre Google Drive

Qu’est ce que les chemins et comment fonctionnent ils?
Un chemin est une chaîne de caractères qui indique l'emplacement d'un fichier ou d'un répertoire sur votre système de fichiers. Il peut être absolu (à partir de la racine du système de fichiers) ou relatif (à partir de votre répertoire de travail actuel). Chaque dossier est séparé par un slash ‘/‘
Chemins absolus : Ils spécifient l'emplacement complet, par exemple : /content/drive/My Drive/LP/. Ici, le dossier ‘LP’ est contenu dans le dossier ‘My Drive’, lui même contenu dans le dossier ‘drive’, et ainsi de suite.