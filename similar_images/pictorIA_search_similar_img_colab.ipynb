{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "55cad632",
   "metadata": {},
   "source": [
    "## Introduction"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e873ad30",
   "metadata": {},
   "source": [
    "This notebook demonstrates how to use deep learning to find images similar to a given image within a dataset. The process involves comparing feature vectors extracted from the images."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9a1b3d08",
   "metadata": {},
   "source": [
    "## Running this Notebook in Google Colab\n",
    "\n",
    "This notebook is designed to be run in Google Colab:\n",
    "1. Download the notebook to your local machine.\n",
    "2. In Google Colab, select 'File' > 'Import notebook'.\n",
    "3. Drop or select the uploaded the notebook."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ae86c3e4-d6c8-4a55-a566-94df7c7342c0",
   "metadata": {},
   "source": [
    "## Switching to GPU Runtime\n",
    "\n",
    "To leverage GPU acceleration in Google Colab, you need to switch the runtime type of your notebook from CPU to GPU. Follow these steps to change the runtime type:\n",
    "\n",
    "1. Click on 'Runtime' in the menu bar at the top of your notebook.\n",
    "2. Select 'Change runtime type' from the dropdown menu.\n",
    "3. In the dialog that appears, under 'Hardware accelerator', select 'GPU' from the dropdown list.\n",
    "4. Click 'Save' to apply the changes.\n",
    "\n",
    "After changing the runtime, your notebook environment will restart. This is normal and means that the GPU is now available for use.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cc8141a7-b87f-413a-9065-f3c5fda722ca",
   "metadata": {},
   "source": [
    "## Mounting Google Drive in Colab\n",
    "To access files and folders from your Google Drive in Google Colab, you need to mount your Google Drive in the runtime environment. This will allow you to read and write files just like you would on your local machine. Run the following cell to mount your Google Drive. After running the cell, you will see a link to authorize Colab to access your Google Drive. Click the link, choose your Google account, and give access to your Google Drive."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1c854594",
   "metadata": {},
   "outputs": [],
   "source": [
    "from google.colab import drive\n",
    "drive.mount('/content/drive')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "88c7bcaf",
   "metadata": {},
   "source": [
    "## Import Libraries\n",
    "First, we import the necessary Python libraries for processing images and calculating similarities."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "49ce28b6-a3e8-4607-aa1e-6ce452484c92",
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "import json\n",
    "import numpy as np\n",
    "from tensorflow.keras.applications.resnet50 import ResNet50, preprocess_input\n",
    "from tensorflow.keras.preprocessing import image\n",
    "from tqdm import tqdm\n",
    "from concurrent.futures import ThreadPoolExecutor"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ca0013ec",
   "metadata": {},
   "source": [
    "## Extract and store feature vectors\n",
    "We load a pre-trained ResNet50 model from TensorFlow. This model is utilized to extract feature vectors from images in a folder and store them in a json file. Change the directories to match your folder structure (for example /content/drive/images/)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4212ecbb-bfa7-4508-952d-66e941bafcb1",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define the extract_features_batch function \n",
    "def extract_features_batch(img_paths, model, batch_size=32):\n",
    "    batch_features = []\n",
    "    for i in range(0, len(img_paths), batch_size):\n",
    "        batch_paths = img_paths[i:i + batch_size]\n",
    "        batch_images = []\n",
    "        for img_path in batch_paths:\n",
    "            img = image.load_img(img_path, target_size=(224, 224))\n",
    "            img_array = image.img_to_array(img)\n",
    "            batch_images.append(img_array)\n",
    "        batch_images = np.array(batch_images)\n",
    "        batch_images = preprocess_input(batch_images)\n",
    "        batch_preds = model.predict(batch_images, verbose=0)\n",
    "        batch_features.extend(batch_preds)\n",
    "    return batch_features\n",
    "\n",
    "# Define the process_images_in_batches function \n",
    "def process_images_in_batches(image_folder, output_file, model, batch_size=32):\n",
    "    # Gather all image paths\n",
    "    img_paths = [os.path.join(image_folder, f) for f in os.listdir(image_folder) if f.lower().endswith(('.jpg', '.png'))]\n",
    "    total_batches = (len(img_paths) + batch_size - 1) // batch_size  # Calculate the total number of batches\n",
    "\n",
    "    embeddings = {}\n",
    "\n",
    "    # Process images in batches\n",
    "    for i in tqdm(range(0, len(img_paths), batch_size), total=total_batches, desc=\"Processing batches\", leave=False):\n",
    "        batch_paths = img_paths[i:i + batch_size]\n",
    "        batch_features = extract_features_batch(batch_paths, model, batch_size)\n",
    "\n",
    "        # Map back the features to their respective image paths\n",
    "        for img_path, features in zip(batch_paths, batch_features):\n",
    "            embeddings[os.path.basename(img_path)] = features.tolist()\n",
    "\n",
    "    # Save embeddings for later use\n",
    "    with open(output_file, 'w') as f:\n",
    "        json.dump(embeddings, f)\n",
    "\n",
    "model = ResNet50(weights='imagenet', include_top=False, pooling='max')\n",
    "# Adjust the path as necessary, the embeddings file can be stored in the same folder as the images\n",
    "process_images_in_batches('PATH_TO_IMAGES_FOLDER', 'PATH_TO_EMBEDDINGS_FOLDER/embeddings.json', model, batch_size=32)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a06f2bee",
   "metadata": {},
   "source": [
    "## Compare image vectors\n",
    "This cell loads precomputed embeddings for a dataset stored in a JSON file, defines the compare_image function (which processes an uploaded image, compares it with the precomputed embeddings using cosine similarity, and ranks the dataset images based on this similarity) and sets up a Gradio interface to allow users to upload an image and view images from the dataset that are similar to it. The interface can also be accessed through a local URL. It can be used with an existing embeddings file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7016a5f4-275a-4420-9b09-bad797ece2c5",
   "metadata": {},
   "outputs": [],
   "source": [
    "!pip install gradio"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a1d0b662-df7c-45ca-83c8-8e22572f67ff",
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "import gradio as gr\n",
    "import json\n",
    "import numpy as np\n",
    "from scipy.spatial.distance import cosine\n",
    "from tensorflow.keras.applications.resnet50 import ResNet50, preprocess_input\n",
    "from tensorflow.keras.preprocessing import image\n",
    "from PIL import Image\n",
    "\n",
    "# Load the pre-trained ResNet50 model for feature extraction\n",
    "model = ResNet50(weights='imagenet', include_top=False, pooling='max')\n",
    "\n",
    "# Load stored embeddings, adjust the path as necessary\n",
    "with open('PATH_TO_EMBEDDINGS_FOLDER/embeddings.json', 'r') as f:\n",
    "    stored_embeddings = json.load(f)\n",
    "\n",
    "def compare_image(input_img):\n",
    "    # Ensure input_img is a PIL Image and resize it\n",
    "    if not isinstance(input_img, Image.Image):\n",
    "        input_img = Image.fromarray(input_img.astype('uint8'), 'RGB')\n",
    "    input_img = input_img.resize((224, 224))\n",
    "    \n",
    "    # Convert the image to a format expected by the model\n",
    "    input_img_array = image.img_to_array(input_img)\n",
    "    input_img_array = np.expand_dims(input_img_array, axis=0)\n",
    "    input_img_array = preprocess_input(input_img_array)\n",
    "\n",
    "    # Extract features of the uploaded image\n",
    "    input_features = model.predict(input_img_array).flatten()\n",
    "\n",
    "    # Compare with stored embeddings\n",
    "    similarities = {}\n",
    "    for img_name, stored_embedding in stored_embeddings.items():\n",
    "        similarity = 1 - cosine(input_features, np.array(stored_embedding))\n",
    "        similarities[img_name] = similarity\n",
    "\n",
    "    # Sort images by similarity\n",
    "    sorted_similarities = sorted(similarities.items(), key=lambda item: item[1], reverse=True)\n",
    "    \n",
    "    # Prepare outputs\n",
    "    top_n = 5  # Adjust based on how many results you want to show\n",
    "    # Adjust the path as necessary\n",
    "    image_paths = [f\"PATH_TO_IMAGES_FOLDER/{img_name}\" for img_name, _ in sorted_similarities[:top_n]]  \n",
    "    labels = [f\"{img_name}: {score:.2f}\" for img_name, score in sorted_similarities[:top_n]]\n",
    "\n",
    "    return image_paths, \"\\n\".join(labels)  # Return for Gradio output\n",
    "\n",
    "# Define the Gradio interface\n",
    "iface = gr.Interface(\n",
    "    fn=compare_image,\n",
    "    inputs=gr.Image(),\n",
    "    outputs=[gr.Gallery(label=\"Similar Images\"), gr.Text(label=\"Image Names and Scores\")],\n",
    "    title=\"Image Similarity\",\n",
    "    description=\"Upload an image to find the most similar images in the dataset.\"\n",
    ")\n",
    "iface.launch()\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
