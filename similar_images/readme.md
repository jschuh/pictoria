# Image Similarity Search with Deep Learning

This project demonstrates how to use deep learning to find images similar to a given image within a dataset. It leverages a pre-trained ResNet50 model for feature extraction and compares images based on their feature vectors.

## Installation

You can install all dependencies using the following command in your terminal or command prompt:

```bash
pip install -r requirements.txt
```

This command should be run in the same directory where your requirements.txt file is located, or you should specify the path to the file in the command.
Alternatively, you can use the Google Colab version to run the scritp online.

## Usage

Run the Jupyter notebook to explore the functionality:

```bash
jupyter notebook pictorIA_search_similar_img.ipynb
```

Or:

```bash
jupyter lab pictorIA_search_similar_img.ipynb
```

Follow the instructions within the notebook for uploading images and viewing the most similar images from the dataset.

## Credits

This script was created by Julien Schuh in the pictorIA Huma-Num Consortium. For more information, visit [pictorIA](https://pictoria.hypotheses.org/).

## License

MIT License
